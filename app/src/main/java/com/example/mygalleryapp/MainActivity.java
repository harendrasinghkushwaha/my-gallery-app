package com.example.mygalleryapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.viewpager2.widget.ViewPager2;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.Image;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnitemClcikListerner{
    CustomAdapter customAdapter;
    ArrayList<String> mList;
    ArrayList<String> galleryImageUrls;
    //ViewPager2 viewPager2;
    RecyclerView recyclerView;
    SnapHelper snapHelper = new PagerSnapHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        CheckUserPermsions();
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        snapHelper.attachToRecyclerView(recyclerView);
        mList = new ArrayList<>();
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        //itemTouchHelper.onChildViewAttachedToWindow(viewPager2);
        //itemTouchHelper.attachToRecyclerView(recyclerView);
        itemTouchHelper.attachToRecyclerView(recyclerView);


    }



    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.UP) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            switch (direction){
                case ItemTouchHelper.UP:
                    //Toast.makeText(getApplicationContext(),"Hello Javatpoint",Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(),"Position"+position,Toast.LENGTH_SHORT).show();
                    galleryImageUrls.remove(position);
                    customAdapter.notifyItemRemoved(position);
                    //deleteImage(position);
//                    File fdelete = new File(galleryImageUrls.get(position));
//                    if (fdelete.exists()) {
//                        Log.d("MyApp","file found");
//                        if (fdelete.delete()) {
//                            Log.d("MyApp","deletion done");
//                        } else {
//                            Log.d("MyApp","deletion not done");
//                        }
//                    }else{
//                        Log.d("MyApp","file not found");
//                    }
                    break;
            }

        }
    };

//    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.UP) {
//        @Override
//        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//            return false;
//        }
//
//        @Override
//        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
//            int position = viewHolder.getAdapterPosition();
//            switch (direction){
//                case ItemTouchHelper.UP:
//                    //Toast.makeText(getApplicationContext(),"Hello Javatpoint",Toast.LENGTH_SHORT).show();
//                    //Toast.makeText(getApplicationContext(),"Position"+position,Toast.LENGTH_SHORT).show();
//                    //galleryImageUrls.remove(position);
//                    //customAdapter.notifyItemRemoved(position);
//                    deleteImage(position);
////                    File fdelete = new File(galleryImageUrls.get(position));
////                    if (fdelete.exists()) {
////                        Log.d("MyApp","file found");
////                        if (fdelete.delete()) {
////                            Log.d("MyApp","deletion done");
////                        } else {
////                            Log.d("MyApp","deletion not done");
////                        }
////                    }else{
////                        Log.d("MyApp","file not found");
////                    }
////                    break;
//            }
//
//        }
//    };

//    public void deleteImage(int position) {
//        String file_dj_path = Environment.getExternalStorageDirectory() + galleryImageUrls.get(position);//"/ECP_Screenshots/abc.jpg";
//        File fdelete = new File(file_dj_path);
//        if (fdelete.exists()) {
//            Log.d("MyApp","file found");
//            if (fdelete.delete()) {
//                Log.e("-->", "file Deleted :" + file_dj_path);
//                callBroadCast();
//            } else {
//                Log.e("-->", "file not Deleted :" + file_dj_path);
//            }
//        }
//    }

//    public void callBroadCast() {
//        if (Build.VERSION.SDK_INT >= 14) {
//            Log.e("-->", " >= 14");
//            MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
//                /*
//                 *   (non-Javadoc)
//                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
//                 */
//                public void onScanCompleted(String path, Uri uri) {
//                    Log.e("ExternalStorage", "Scanned " + path + ":");
//                    Log.e("ExternalStorage", "-> uri=" + uri);
//                }
//            });
//        } else {
//            Log.e("-->", " < 14");
//            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
//                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
//        }
//    }


    void CheckUserPermsions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }

        display();  // init the contact list

    }




    //get acces to location permsion
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    display();  // init the contact list
                } else {
                    // Permission Denied
                    Toast.makeText(this, "your message", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

//    private ArrayList<File> findImage(File file) {
//        ArrayList<File> imageList=new ArrayList<>();
//        File[] imageFile =  file.listFiles();
//        for (File singleimage : imageFile){
//            if (singleimage.isDirectory() && !singleimage.isHidden()){
//                imageList.addAll(findImage(singleimage));
//            }else {
//                if (singleimage.getName().endsWith(".jpg") ||
//                        singleimage.getName().endsWith(".png") ||
//                        singleimage.getName().endsWith(".webp") ||
//                        singleimage.getName().endsWith(".jpeg") ||
//                        singleimage.getName().endsWith(".gif") ||
//                        singleimage.getName().endsWith(".bmp")
//                ){
//                    imageList.add(singleimage);
//                }
//            }
//        }
//        return  imageList;
//    }

    private void display() {
        //myimageFile = findImage(Environment.getExternalStorageDirectory());

        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};//get all columns of type images
        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;//order data by date

        Cursor imagecursor = this.managedQuery(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");//get all data in Cursor by sorting in DESC order

        galleryImageUrls = new ArrayList<String>();

        for (int i = 0; i < imagecursor.getCount(); i++) {
            imagecursor.moveToPosition(i);
            int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA);//get column index
            galleryImageUrls.add(imagecursor.getString(dataColumnIndex));//get Image from column index
            customAdapter=new CustomAdapter(galleryImageUrls, (OnitemClcikListerner) this);
            recyclerView.setAdapter(customAdapter);
            //recyclerView.setLayoutManager(new GridLayoutManager(this,3));

        }
//        for (int j=0;j<myimageFile.size();j++){
//            mList.add(String.valueOf(myimageFile.get(j)));
//            customAdapter=new CustomAdapter(mList, (OnitemClcikListerner) this);
//            recyclerView.setAdapter(customAdapter);
//            //recyclerView.setLayoutManager(new GridLayoutManager(this,3));
//            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//
//        }
    }

    @Override
    public void onClick(int position) {

        Toast.makeText(this, "Postion: "+position, Toast.LENGTH_SHORT).show();
//        Intent intent=new Intent(MainActivity.this,FullImageActivity.class);
//        intent.putExtra("image",String.valueOf(myimageFile.get(position)));
//        intent.putExtra("pos",position);
//        intent.putExtra("imageList",myimageFile);
//        startActivity(intent);

    }


}